// The lert program periodically checks a URL, possibly including a per-request random
// number denoted by $RAND, and sends an SMS alert to the given phone number if it is
// down.

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
)

var accountSid = flag.String("account_sid", "", "Account SID for Twilio")
var authToken = flag.String("auth_token", "", "Auth token for Twilio")
var urlToCheck = flag.String("url", "", "URL to check. $RANDOM gets replaced with a random number, different each request")
var from = flag.String("from", "", "phone number to send SMS from")
var to = flag.String("to", "", "phone number to send SMS to")
var interval = flag.Duration("interval", time.Minute, "how long to wait between checks")

func main() {
	flag.Parse()
	if *accountSid == "" {
		log.Fatal("missing flag: --acount_sid")
	}
	if *authToken == "" {
		log.Fatal("missing flag: --auth_token")
	}
	if *urlToCheck == "" {
		log.Fatal("missing flag: --url")
	}
	if *from == "" {
		log.Fatal("missing flag: --from")
	}
	if *to == "" {
		log.Fatal("missing flag: --to")
	}

	rand.Seed(time.Now().UnixNano())
	for {
		r := fmt.Sprintf("%d", rand.Int())
		u := strings.Replace(*urlToCheck, "$RAND", r, -1)
		resp, err := http.Get(u)
		if err != nil {
			msg := fmt.Sprintf("failed to GET %s: %v", u, err)
			log.Printf("Sending alert: '%s'", msg)
			if err := alert(msg); err != nil {
				log.Printf("sending alert: %v", err)
			}
		} else if resp.StatusCode != http.StatusOK {
			msg := fmt.Sprintf("ALERT: got status %s for %s", resp.Status, u)
			log.Printf("DOWN: Sending alert: '%s'", msg)
			if err := alert(msg); err != nil {
				log.Printf("sending alert: %v", err)
			}
		} else {
			log.Printf("OK: %s", u)
		}
		time.Sleep(time.Minute)
	}
}

func alert(msg string) error {

	// Build out the data for our message
	v := url.Values{}
	v.Set("From", *from)
	v.Set("To", *to)
	v.Set("Body", msg)
	rb := *strings.NewReader(v.Encode())

	// Create client
	client := &http.Client{}

	urlStr := "https://api.twilio.com/2010-04-01/Accounts/" + *accountSid + "/Messages.json"
	req, err := http.NewRequest("POST", urlStr, &rb)
	if err != nil {
		return errors.Wrapf(err, "POSTing %s", urlStr)
	}
	req.SetBasicAuth(*accountSid, *authToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	// Make request
	resp, err := client.Do(req)
	if err != nil {
		return errors.Wrapf(err, "POST request to %s failed", urlStr)
	}
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		var data map[string]interface{}
		bodyBytes, _ := ioutil.ReadAll(resp.Body)
		err := json.Unmarshal(bodyBytes, &data)
		if err != nil {
			return errors.Wrap(err, "mal-formed json response to SMS alert request")
		}
	} else {
		return errors.Wrapf(err, "got non-2xx response code: %s", resp.Status)
	}
	return nil
}
